﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;


public class KnobRotator : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler {


    public Image _bg;
    public RectTransform _knobRotator;

    public float _angle = 90.0f;

    private Vector3 inputVector;

    public GameObject andy;

    public KnobTranslator knobTranslator;

	// Use this for initialization
	void Start () {
        ChangeAngle(_angle);
        Debug.Log("Rotator");

	}
	
	// Update is called once per frame
	void Update () {
        //ChangeAngle(_angle);
	}

    private void ChangeAngle(float angle) {
        _knobRotator.localEulerAngles = new Vector3(0, 0, angle);
    }




    public void OnDrag(PointerEventData eventData)
    {
        Vector2 pos;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_bg.rectTransform, eventData.position, eventData.pressEventCamera, out pos))
        {
            pos.x = (pos.x / _bg.rectTransform.sizeDelta.x);
            pos.y = (pos.y / _bg.rectTransform.sizeDelta.y);
                       


            inputVector = new Vector3(pos.x * 2, 0, pos.y * 2);
            inputVector = inputVector.magnitude > 1.0f ? inputVector.normalized : inputVector;


            _angle = Mathf.Atan2(pos.y, pos.x) * 180 / Mathf.PI;

            // Checking if the angle is in negative then convert it to positve angle
            _angle = _angle < 0 ? _angle + 360 : _angle;
           

            // DOnt understand the logic here :D, Aditya knows why it works, it works tho 
            ChangeAngle(_angle + 180);


            Debug.Log(_angle);


            andy.transform.rotation = Quaternion.identity;
            andy.transform.Rotate(0, -_angle, 0);

            knobTranslator.initialRotation = andy.transform.rotation;

        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        OnDrag(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        inputVector = Vector3.zero;

    }


}
