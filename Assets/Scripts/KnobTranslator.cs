﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;


public class KnobTranslator : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{


    public Image _bg;
    public RectTransform _knobTranslator;

    public float _angle = 270.0f;
    //private float _previousAngle = 0.0f;

    private Vector3 inputVector;

    public GameObject dirIndicator;
    public GameObject child;

    public Quaternion initialRotation;


    //public GameObject andy;



    // Use this for initialization
    void Start()
    {
        ChangeAngle(_angle);

        Debug.Log("Translator");

        initialRotation = child.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        //ChangeAngle(_angle);
    }

    void LateUpdate() {
        child.transform.rotation = initialRotation;
    }

    private void ChangeAngle(float angle)
    {
        _knobTranslator.localEulerAngles = new Vector3(0, 0, angle);
    }




    public void OnDrag(PointerEventData eventData)
    {
        Vector2 pos;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_bg.rectTransform, eventData.position, eventData.pressEventCamera, out pos))
        {
            pos.x = (pos.x / _bg.rectTransform.sizeDelta.x);
            pos.y = (pos.y / _bg.rectTransform.sizeDelta.y);



            inputVector = new Vector3(pos.x * 2, 0, pos.y * 2);
            inputVector = inputVector.magnitude > 1.0f ? inputVector.normalized : inputVector;


            _angle = Mathf.Atan2(pos.y, pos.x) * 180 / Mathf.PI;

            // Checking if the angle is in negative then convert it to positve angle
            _angle = _angle < 0 ? _angle + 360 : _angle;


            // DOnt understand the logic here :D, Aditya knows why it works, it works tho 
            ChangeAngle(_angle);


            //_previousAngle = _angle - _previousAngle;

            Debug.Log(_angle);


            dirIndicator.transform.rotation = Quaternion.identity;
            dirIndicator.transform.Rotate(90, -_angle, 0);


        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //we will do the same sa OnDrag()



        OnDrag(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {

        // Nothing to do here for now
        inputVector = Vector3.zero;

    }


    public float getAngle() {
        return _angle;
    }
}


