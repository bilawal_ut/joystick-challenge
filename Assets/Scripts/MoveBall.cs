﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBall : MonoBehaviour {
    
    public Vector3 MoveVector { set; get; }

    public VirtualJoyStick joyStick;

    public GameObject andy;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        MoveVector = PoolInput();
        Move();
	}

    private void Move()
    {
        //andy.transform.Translate(MoveVector.z,0f,0f);
        andy.transform.Translate(Vector3.right * Time.deltaTime * (MoveVector.z/5));
    }

    private Vector3 PoolInput()
    {
        Vector3 dir = Vector3.zero; 

        dir.x = joyStick.Horizontal();
        dir.z = joyStick.Vertical();

        if (dir.magnitude > 1)
            dir.Normalize();

        return dir;
    }
}
