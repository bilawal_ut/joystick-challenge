﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

public class VirtualJoyStick : MonoBehaviour,IDragHandler, IPointerDownHandler, IPointerUpHandler {


    #region variables

    private Image bgImage;
    private Image jsImage;
    private Vector3 inputVector;

    #endregion


    // Use this for initialization
    void Start () {
        bgImage = GetComponent<Image>();
        jsImage = transform.GetChild(0).GetComponent<Image>();
    }
    
    // Update is called once per frame
    void Update () {
        
    }


    public virtual void OnDrag(PointerEventData eventData)
    {
        Vector2 pos;
        if(RectTransformUtility.ScreenPointToLocalPointInRectangle(bgImage.rectTransform, eventData.position, eventData.pressEventCamera,out pos)){
            pos.x = (pos.x / bgImage.rectTransform.sizeDelta.x);
            pos.y = (pos.y / bgImage.rectTransform.sizeDelta.y);

            inputVector = new Vector3(pos.x * 2 , 0, pos.y * 2);
            inputVector = inputVector.magnitude > 1.0f ? inputVector.normalized : inputVector;

            jsImage.rectTransform.anchoredPosition = new Vector3(inputVector.x * (bgImage.rectTransform.sizeDelta.x / 2),
                                                                 inputVector.z * (bgImage.rectTransform.sizeDelta.y / 2));

            //Debug.Log(inputVector );
        }
    }

    public virtual void OnPointerDown(PointerEventData eventData)
    {
        OnDrag(eventData);
    }

    public virtual void OnPointerUp(PointerEventData eventData)
    {
        inputVector = Vector3.zero;
        jsImage.rectTransform.anchoredPosition = Vector3.zero;
    }

    public float Horizontal() {
        if (inputVector.x != 0)
            return inputVector.x;
        else
            return Input.GetAxis("Horizontal");
        
    }


    public float Vertical()
    {
        if (inputVector.z != 0)
            return inputVector.z;
        else
            return Input.GetAxis("Vertical");

    }
}
