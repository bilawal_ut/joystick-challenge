﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleARCore;

public class TextEditScript : MonoBehaviour {


    // Our final annotation string, that is entered by the user
    private string annotation = "";

    //Ref to the keyboard
    private TouchScreenKeyboard keyboard;

    //bool to check if the keyboard is open, if 'true' then we will listen to the Done status to capture the text
    private bool iskeyBoardOpen = false;

    private GameObject annotaionText;

    TextMesh textMesh;
    MeshRenderer meshRenderer;

    public GameObject annotationPrefab;

    private GameObject textObject;

    private Quaternion placementRotation;
    private Vector3 placementPosition;

    void Awake()
    {
        annotaionText = new GameObject();
        textMesh = annotaionText.AddComponent<TextMesh>();
        meshRenderer = annotaionText.AddComponent<MeshRenderer>();
    }

    void Start()
    {
    }


    void Update()
    {
        if(iskeyBoardOpen){
            captureAnnotation();
        }
    }

    private void captureAnnotation()
    {

        // if done, close the Keyboard

        if (keyboard.status == TouchScreenKeyboard.Status.Done)
        {
            annotation = keyboard.text;
            iskeyBoardOpen = false;
           
            // Here place the 3d text in AR

            textMesh.text=annotation;
            textMesh.fontSize = 500;

            Debug.Log(annotation);

            textObject = Instantiate(annotationPrefab, new Vector3(placementPosition.x+ 120, placementPosition.y + 120, placementPosition.z+ 120), placementRotation);
            textObject.transform.localScale= new Vector3(0.004F,0.004F,0.004F);


        }
        else
        {
            // keep listening to the user inputs
            annotation = keyboard.text;
        }
    }


    // This function will be called onClick() of the button
    public void StartCapture() {
        //Open Keyboard
        keyboard = TouchScreenKeyboard.Open(annotation, TouchScreenKeyboardType.Default, false, false, false, false, "Enter annotation");

        // Tell unity to start listening to Done Status of the Keyboard
        iskeyBoardOpen = true;
    }


    public void setCo(Quaternion rotation, Vector3 position) {

        placementPosition = position;
        placementRotation = rotation;

        Debug.Log("-------");
        Debug.Log(position);
        Debug.Log(rotation);
        Debug.Log("-------");
    }


}
